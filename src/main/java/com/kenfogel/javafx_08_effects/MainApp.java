package com.kenfogel.javafx_08_effects;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Example of image effects
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Effects");

        int width = 500;
        int height = 300;
        String path = "/images/javafx-logo.png";

        Pane root = new Pane();
        root.setStyle("-fx-background-color: black;");

        root.getChildren().add(createImageViewEffects(path));
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    /**
     * Create an ImageView
     *
     * @param path
     * @return
     */
    private ImageView createImageViewEffects(String path) {
        Image freeFall = new Image(
                getClass().getResourceAsStream(path));
        ImageView imageView = new ImageView();
        imageView.setLayoutX(50);
        imageView.setLayoutY(50);
        Reflection r = new Reflection();
        r.setFraction(0.9);
        imageView.setEffect(r);
        imageView.setImage(freeFall);
        return imageView;
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
